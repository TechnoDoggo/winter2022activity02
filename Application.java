public class Application
{
	public static void main(String args[])
	{
		Wolf TraitsOne = new Wolf();
		Wolf TraitsTwo = new Wolf();
		Wolf TraitsThree = new Wolf();
		Wolf Actions = new Wolf();
		
		TraitsOne.FavPart = "Face/Snoot";
		TraitsOne.OptimalNum = 2;
		TraitsOne.FavKind = "Red Wolves";
		
		TraitsTwo.FavPart = "Ears";
		TraitsTwo.OptimalNum = 3;
		TraitsTwo.FavKind = "Grey Wolves";
		
		TraitsThree.FavPart = "Tail";
		TraitsThree.OptimalNum = 5;
		TraitsThree.FavKind = "Black Wolves";
		
		Wolf[] Pack = new Wolf[3];
		Pack[0] = TraitsOne;
		Pack[1] = TraitsTwo;
		Pack[2] = TraitsThree;
		
		System.out.println(Pack[2].FavPart);
		
		//Actions.RunningWolves();
		//Actions.FeedingWolves(TraitsOne.OptimalNum);
		
		//System.out.println(TraitsOne.FavPart);
		//System.out.println(TraitsOne.OptimalNum);
	}
}